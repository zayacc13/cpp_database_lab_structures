﻿#include <iostream>
#include <stdio.h>
#include <fstream>
#include <Windows.h>
#include <stdio.h>
#pragma warning (disable:4996)
#define MaxSize 256
#define Len 110
#define CountComm 6
#define VoidSpace 10
#define MaxTypeSize 15
#define MaxFamSize 20
#define MaxWeightSize 20
#define MaxQuantitySize 20




struct FoodProduction
{
    char* fam;
    int type;
    double weight;
    int quant;
    double cost;
};



int FindLenOfNumber(int Number)
{
    int count = 0;
    while (Number != 0)
    {
        Number = int(Number / 10);
        count += 1;
    }

    return count;
}



int IsValidCommandNumber(int c)
{
    while (c > CountComm || c < 1)
    {
        std::cout << "Вы ввели номер несуществующей команды.\n";
        std::cout << "Введите номер команды\t";
        std::cin >> c;
        std::cout << '\n';
    }
    return c;
}



void FillSpace(int SpaceLen)
{
    for (int i = 0; i < SpaceLen; i++)
    {
        std::cout << ' ';
    }
}



int NotCorrectType()
{
    int Number;
    int SpaceLen;
    while (!(std::cin >> Number) || (std::cin.peek() != '\n'))
    {
        std::cout << '\n';
        std::cin.clear();
        while (std::cin.get() != '\n');
        const char* text = "Ошибка ввода. Введите корректное значение";
        std::cout << text;
        SpaceLen = Len - strlen(text) - VoidSpace;
        FillSpace(SpaceLen);

    }

    return Number;
}



int Interface()
{
    int command;
    int SpaceLen = 0;
    const char* Commands[] = { "Добавить информацию о новом товаре" ,
                                "Распечатать информацию о всех товарах в табличном виде",
                                "Поиск изделий по названию",
                                "Фильтр по типу" ,
                                "Сортировать по увеличению стоимости" ,
                                "Сохранить и выйти" };


    for (int i = 0; i < CountComm; i++)
    {
        std::cout << Commands[i];
        //4 - это 3 точки и пробел 
        SpaceLen = Len - strlen(Commands[i]) - 4 - FindLenOfNumber(i + 1);
        for (int j = 0; j < SpaceLen; j++)
        {
            std::cout << ' ';
        }
        std::cout << "... " << i + 1 << '\n';
    }


    command = NotCorrectType();
    std::cout << '\n';

    command = IsValidCommandNumber(command);

    return command;
}



void FillStr()
{
    for (int i = 0; i < Len; i++)
    {
        std::cout << '-';
    }
    std::cout << '\n';
}



void StartInterface()
{
    const char* TypeText = "Тип изделия";
    const char* FamText = "Название изделия";
    const char* WeightText = "Вес одного изделия";
    const char* QuantityText = "Кол-во изделий";
    const char* CostText = "Стоимость одного изделия";
    int SpaceLen = 0;


    std::cout << TypeText;
    FillSpace(MaxTypeSize - strlen(TypeText) - 1);

    std::cout << '|' << FamText;
    FillSpace(MaxFamSize - strlen(FamText));

    std::cout << '|' << QuantityText;
    FillSpace(MaxQuantitySize - strlen(QuantityText));

    std::cout << '|' << WeightText;
    FillSpace(MaxWeightSize - strlen(WeightText));

    std::cout << '|' << CostText;
    FillSpace(Len - MaxWeightSize - MaxQuantitySize - MaxFamSize - MaxTypeSize - strlen(CostText) - 4);
    std::cout << "|\n";

    FillStr();
}



void ProductInfo(FoodProduction* Product, int i)
{
    std::cout << Product[i].type;
    FillSpace(MaxTypeSize - FindLenOfNumber(Product[i].type) - 1);
    std::cout << '|';

    std::cout << Product[i].fam;
    FillSpace(MaxFamSize - strlen(Product[i].fam) );
    std::cout << '|';

    std::cout << Product[i].quant;
    FillSpace(MaxQuantitySize - FindLenOfNumber(Product[i].quant));
    std::cout << '|';

    std::cout << Product[i].weight;
    FillSpace(MaxWeightSize - FindLenOfNumber(Product[i].weight));
    std::cout << '|';

    std::cout << Product[i].cost;
    FillSpace(Len - MaxWeightSize - MaxQuantitySize - MaxFamSize - MaxTypeSize - FindLenOfNumber(Product[i].cost) - 4);
    std::cout << "|\n";
}



int NotCorrectNumber(int c, int a, int b)
{
    while (c < a || c > b)
    {
        c = NotCorrectType();
        while (c < a || c > b)
        {
            const char* text1 = "Введите корректное значение";
            std::cout << text1 << " ( от " << a << " до " << b << " )";
            FillSpace(Len - VoidSpace - strlen(text1) - 12 - FindLenOfNumber(a) - FindLenOfNumber(b));
            std::cin >> c;
        }
    }
    return c;
}




FoodProduction* NewProduct(FoodProduction* DataArray,int &count,std::ofstream& dataW)
{
    char buff[MaxSize];
    int Number;
    int SpaceLen;
    DataArray = (FoodProduction*)realloc(DataArray, (count + 1) * sizeof(FoodProduction));
        
        
    

    const char* t1 = "Введите название товара";
    std::cout << t1;

    SpaceLen = Len - strlen(t1) - VoidSpace;
    FillSpace(SpaceLen);

    std::cin >> buff;
    (DataArray+count)->fam = (char*)malloc((strlen(buff) + 1) * sizeof(char*));
    strcpy((DataArray + count)->fam, buff);
    dataW << buff << ' ';
    std::cout << '\n';


    const char* t2 = "Введите тип изделия ( 1 - булочка; 2 - пирожок; 3 - пирожное )";
    std::cout << t2;

    SpaceLen = Len - strlen(t2) - VoidSpace;
    FillSpace(SpaceLen);

    Number = NotCorrectNumber(5, 1, 3);
    (DataArray + count)->type = Number;
    dataW << Number << ' ';
    std::cout << '\n';


    const char* t3 = "Введите вес одного изделия ( г )";
    std::cout << t3;

    SpaceLen = Len - strlen(t3) - VoidSpace;
    FillSpace(SpaceLen);

    Number = NotCorrectType();
    (DataArray + count)->weight = Number;
    dataW << Number << ' ';
    std::cout << '\n';


    const char* t4 = "Введите кол-во изделий";
    std::cout << t4;

    SpaceLen = Len - strlen(t4) - VoidSpace;
    FillSpace(SpaceLen);

    Number = NotCorrectType();
    (DataArray + count)->quant = Number;
    dataW << Number << ' ';
    std::cout << '\n';


    const char* t5 = "Введите стоимость одного изделия ( руб )";
    std::cout << t5;

    SpaceLen = Len - strlen(t5) - VoidSpace;
    FillSpace(SpaceLen);

    Number = NotCorrectType();
    (DataArray + count)->cost = Number;
    dataW << Number << '\n';
    std::cout << '\n';


    return DataArray;
}



void OutputProductsInfo(FoodProduction* Products, int count)
{
    if (count != 0)
    {
        StartInterface();
        for (int i = 0; i < count; i++)
        {
            ProductInfo(Products, i);
        }
        FillStr();
    }
    else
    {
        std::cout << "На данный момент не было добавлено ни одного товара.\n";
    }
}



void FindByFam(FoodProduction* Products, int count)
{
    const char* text1 = "Введите название товара";
    std::cout << text1;
    int LocalCount = 0;
    FillSpace(Len - strlen(text1) - VoidSpace);
    FoodProduction* SpecificProducts;
    SpecificProducts = (FoodProduction*)malloc(sizeof(FoodProduction));

    char* buff = (char*)malloc(MaxSize*sizeof(char));
    std::cin >> buff;

    for (int i = 0; i < count; i++)
    {
        if (strcmp(Products[i].fam, buff) == 0)
        {
            LocalCount++;
            SpecificProducts[LocalCount - 1] = Products[i];
            SpecificProducts = (FoodProduction*)realloc(SpecificProducts, (LocalCount + 1) * sizeof(FoodProduction));
        }
    }
    if (LocalCount != 0)
    {
        StartInterface();
        for (int i = 0; i < LocalCount; i++)
        {
            ProductInfo(SpecificProducts, i);
        }
        FillStr();
    }
    else
    {
        std::cout << "В базе пока нет товаров с таким названием\n\n";
    }

    free(buff);
    free(SpecificProducts);
}



void SortByType(FoodProduction* Products, int count)
{
    const char* text1 = "Введите тип товара";
    std::cout << text1;
    int LocalCount = 0;
    int Number;
    FillSpace(Len - strlen(text1) - VoidSpace);
   

    Number = NotCorrectNumber(5, 1, 3);
    

    FoodProduction* SpecificProducts;
    SpecificProducts = (FoodProduction*)malloc(sizeof(FoodProduction));


    for (int i = 0; i < count; i++)
    {
        if (Products[i].type == Number)
        {
            LocalCount++;
            SpecificProducts[LocalCount-1] = Products[i];
            SpecificProducts = (FoodProduction*)realloc(SpecificProducts, (LocalCount + 1) * sizeof(FoodProduction));
        }
    }
    if (LocalCount != 0)
    {
        StartInterface();
        for (int i = 0; i < LocalCount; i++)
        {
            ProductInfo(SpecificProducts, i);
        }
        FillStr();
    }
    else
    {
        std::cout << "В базе пока нет товаров такого типа\n\n";
    }

    free(SpecificProducts);
}



void SortByCost(FoodProduction* Products, int count)
{
    FoodProduction buff;

    for (int i = 0; i < count; i++)
    {
        for (int j = 0; j < count - 1; j++)
        {
            if (Products[j].cost > Products[j + 1].cost)
            {
                buff = Products[j + 1];
                Products[j + 1] = Products[j];
                Products[j] = buff;
            }
        }
    }

    OutputProductsInfo(Products, count);
}



std::pair<FoodProduction*,int> ReadStudentsData(std::ifstream& dataR, FoodProduction* Products)
{
    int count = 0;
    int LocalCount = 0;
    int smth;

    FoodProduction* tmp;
    

    while (!dataR.eof())
    {  
        tmp = (FoodProduction*)realloc(Products, (count + 1) * sizeof(FoodProduction));
        if (tmp != NULL)
            Products = tmp;

        char* buff = (char*)malloc(MaxSize * sizeof(char));
        dataR >> buff;
        smth = LocalCount % 5;

        switch (smth)
        {
        case 0:
            Products[count].fam = buff;
            break;

        case 1:

            Products[count].type = std::atoi(buff);
            break;

        case 2:

            Products[count].weight = std::atoi(buff);
            break;

        case 3:

            Products[count].quant = std::atoi(buff);
            break;

        case 4:

            Products[count].cost = std::atoi(buff);
            break;
        free(buff);
        free(tmp);
        }

        LocalCount++;

        if (LocalCount >= 5 && LocalCount % 5 == 0) { count++; }
    }

    dataR.close();
    
    return std::pair<FoodProduction*,int>(Products,count);
}





int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    int count = 0;
    
    FoodProduction* DataArray;
    DataArray = (FoodProduction*)malloc(count*sizeof(FoodProduction));

    int command;
    bool flag = true;
    

    std::ofstream dataW;


    std::ifstream dataR;
    dataR.open("data2.txt");
    if (!dataR.is_open())
    {
        std::cout << "Ошибка открытия файла для чтения\n\n";
    }
    else
    {
        std::cout << "Файл для чтения открыт успешно\n\n";
    }

    std::pair<FoodProduction*, int> Smth = ReadStudentsData(dataR, DataArray);
    DataArray = std::get<0>(Smth);
    count = std::get<1>(Smth);


    while (flag)
    {
        command = Interface();

        switch (command)
        {
        case 1:

            dataW.open("data2.txt", std::ofstream::app);
            if (!dataW.is_open())
            {
                std::cout << "Ошибка открытия файла для записи\n\n";
            }
            else
            {
                std::cout << "Файл для записи открыт успешно\n\n";
            }

            DataArray = NewProduct(DataArray,count,dataW);
            count++;
            dataW.close();
            
            break;

        case 2:

            OutputProductsInfo(DataArray, count);
            break;

        case 3:

            FindByFam(DataArray, count);
            break;

        case 4:

            SortByType(DataArray, count);
            break;

        case 5:

            SortByCost(DataArray, count);
            break;

        case 6:

            for (int i = 0; i < count; i++)
            {
                free(DataArray[i].fam);
            }

            free(DataArray);
            flag = false;
            break;

        }
    }
}